import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonText, IonFooter } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab2.css';
import Liste2 from '../components/Liste2';
import fakeData from '../components/fakedata.json';
import Geoloc from '../components/Geoloc';


const Tab2: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Nombre total de morts du COVID-19 par pays</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 2</IonTitle>
          </IonToolbar>
        </IonHeader>
        <Geoloc/>
        <Liste2 data={fakeData.Countries} filtre="" />
        <IonFooter>
          Application de qualité, conçue avec {"<"}3<br />
          Code source sur{" "}
          <a href="https://bitbucket.org/olevitt/formations">
            https://bitbucket.org/olevitt/formations
          </a>
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
