
import React from 'react';
import './Footer.css';

const Footer: React.FC = () => {
    return (
      <p>
        Application de qualité, conçue avec {"<"}3<br />
        Code source sur{" "}
        <a href="https://bitbucket.org/olevitt/formations">
          https://bitbucket.org/olevitt/formations
        </a>
      </p>
    );
  };
  

export default Footer;