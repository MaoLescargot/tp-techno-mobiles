import React, { constructor } from 'react';
import { IonButton } from '@ionic/react';
import {Geolocation} from '@ionic-native/geolocation';
import {Vibration} from '@ionic-native/vibration';


const Geoloc = () => 
<IonButton 
    color='danger' 
    onClick={() => {
        Geolocation.getCurrentPosition().then((geoloc) => {
            console.log(geoloc.coords.latitude + " " + geoloc.coords.longitude);
            console.log(geoloc.coords);
        });
        Vibration.vibrate(100)
    }}> Localise-moi
</IonButton>;

export default Geoloc;